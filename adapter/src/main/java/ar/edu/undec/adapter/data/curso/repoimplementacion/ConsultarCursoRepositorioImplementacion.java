package ar.edu.undec.adapter.data.curso.repoimplementacion;

import curso.modelo.Curso;
import curso.output.ConsultarCursoRepositorio;

import java.util.Collection;

public class ConsultarCursoRepositorioImplementacion {
    private ConsultarCursoRepositorioImplementacion consultarCursoRepositorioImplementacion;
    public Collection<Curso> obtenerCurso() {
        return consultarCursoRepositorioImplementacion.obtenerCurso();
    }
}
