package ar.edu.undec.adapter.data.curso.crud;

import ar.edu.undec.adapter.data.curso.repoimplementacion.ConsultarCursoRepositorioImplementacion;
import curso.modelo.Curso;
import curso.output.ConsultarCursoRepositorio;

import java.util.Collection;

public class ConsultarCursoCRUD {
    private ConsultarCursoRepositorioImplementacion consultarCursoRepositorioImplementacion;
    public Collection<Curso> findAll() {
        return consultarCursoRepositorioImplementacion.obtenerCurso();
    }
}
