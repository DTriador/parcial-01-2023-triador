package curso.exception;

public class CursoIncompletoException extends RuntimeException{
    public CursoIncompletoException(String mensaje){
        super(mensaje);
    }
}
