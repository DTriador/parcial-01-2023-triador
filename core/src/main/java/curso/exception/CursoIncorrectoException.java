package curso.exception;

public class CursoIncorrectoException extends RuntimeException{
    public CursoIncorrectoException(String mensaje){
        super(mensaje);
    }
}
