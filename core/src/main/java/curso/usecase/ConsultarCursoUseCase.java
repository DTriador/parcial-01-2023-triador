package curso.usecase;

import curso.input.ConsultarCursoInput;
import curso.input.CrearCursoInput;
import curso.modelo.Curso;
import curso.output.ConsultarCursoRepositorio;
import curso.output.CrearCursoRepositorio;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;

public class ConsultarCursoUseCase implements ConsultarCursoInput{

    private ConsultarCursoRepositorio consultarCursoRepositorio;
    public ConsultarCursoUseCase(ConsultarCursoRepositorio consultarCursoRepositorio) {
        this.consultarCursoRepositorio = consultarCursoRepositorio;
    }

    @Override
    public Collection<Curso> consultarCurso() {

        return consultarCursoRepositorio.obtenerCurso();
    }
}
