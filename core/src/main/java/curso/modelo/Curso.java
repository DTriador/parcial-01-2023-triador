package curso.modelo;

import curso.exception.CursoIncompletoException;

import java.time.LocalDate;

public class Curso {
    /*"id": null,
  "nombre": "Clean Architecture",
  "fecha_cierre_inscripcion": "2023-03-01T10:00:00.000Z",
  "nivel": "Inicial"*/
    private Integer id;
    private String nombre;
    private LocalDate fecha_cierre_inscripcion;
    private String nivel;

    public Curso(Integer id, String nombre, LocalDate fecha_cierre_inscripcion, String nivel) {
        this.id = id;
        this.nombre = nombre;
        this.fecha_cierre_inscripcion = fecha_cierre_inscripcion;
        this.nivel = nivel;
    }
    public static Curso instancia(Integer id, String nombre, LocalDate fecha_cierre_inscripcion, String nivel) throws RuntimeException{
        if(nombre == null){
            throw new CursoIncompletoException("El nombre del Curso es obligatorio");
        }
        if(nombre.isEmpty()){
            throw new CursoIncompletoException("El nombre del Curso es obligatorio");
        }
        if(fecha_cierre_inscripcion == null){
            throw new CursoIncompletoException("La fecha de cierre del Curso es obligatoria");
        }
        if(nivel == null){
            throw new CursoIncompletoException("El nivel del curso no puede ser null");
        }
        if(nivel.isEmpty()){
            throw new CursoIncompletoException("El nivel del curso no puede estar vacio");
        }
        return new Curso(id,nombre,fecha_cierre_inscripcion,nivel);
    }
}
