package curso.output;

import curso.modelo.Curso;

import java.util.Collection;
import java.util.Collections;

public interface ConsultarCursoRepositorio {
    Collection<Curso> obtenerCurso();

}
